package net.ruigeng.quinntas.data.local;

import net.ruigeng.quinntas.models.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rui.geng on 5/7/17.
 */

public class DataServiceLocal {

    private List<Item> items = new ArrayList<>();

    private static DataServiceLocal SERVICE_INSTANCE;

    private DataServiceLocal() {}

    public static DataServiceLocal getInstance() {
        if(SERVICE_INSTANCE == null) {
            SERVICE_INSTANCE = new DataServiceLocal();
        }

        return SERVICE_INSTANCE;
    }

    public List<Item> getItems() {
        return items;
    }

    public void cacheItems(List<Item> itemList) {
        items.clear();
        items.addAll(itemList);
    }
}
