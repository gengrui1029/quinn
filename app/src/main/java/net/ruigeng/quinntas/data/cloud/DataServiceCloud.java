package net.ruigeng.quinntas.data.cloud;

import net.ruigeng.quinntas.data.DataSource;
import net.ruigeng.quinntas.models.ItemAll;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by rui.geng on 5/7/17.
 */

public class DataServiceCloud {

    private static String BASE_URL = "https://g525204.github.io/";

    private static DataServiceCloud INSTANCE;

    private DataServiceCloud() {
    }

    public static DataServiceCloud getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DataServiceCloud();
        }

        return INSTANCE;
    }

    interface WebAPIService {
        @GET("recipes.json")
        Call<ItemAll> readData();
    }

    /***
     * Read the data from backend.
     * @param callback The callback
     */
    public void readData(final DataSource.LoadDataCallback callback) {
        // TODO: 10/7/17 This part can be optimised in the future, as the Retrofit can be inited in the constructor.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WebAPIService service = retrofit.create(WebAPIService.class);
        Call<ItemAll> jsonCall = service.readData();
        jsonCall.enqueue(new Callback<ItemAll>() {
            @Override
            public void onResponse(Call<ItemAll> call, Response<ItemAll> response) {
                callback.onDataLoaded(response.body().getItems());
            }

            @Override
            public void onFailure(Call<ItemAll> call, Throwable t) {
                callback.onDataFailed();
            }
        });
    }

}
