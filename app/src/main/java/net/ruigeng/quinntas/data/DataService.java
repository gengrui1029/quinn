package net.ruigeng.quinntas.data;

import net.ruigeng.quinntas.data.cloud.DataServiceCloud;
import net.ruigeng.quinntas.data.local.DataServiceLocal;
import net.ruigeng.quinntas.models.Item;

import java.util.List;

/**
 * Created by rui.geng on 5/7/17.
 * Data Repository class.
 */

public class DataService {

    private static DataService INSTANCE;

    private DataService() {}

    public static DataService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new DataService();
        }

        return INSTANCE;
    }

    /***
     * Get the items from data source. It firstly check if the data has been cached, if not, load
     * the data from backend.
     * @param callback  The callback.
     */
    public void getItems(final DataSource.LoadDataCallback callback) {
        List<Item> items = DataServiceLocal.getInstance().getItems();

        if(items.isEmpty()) { // If there is no data been cached.
            // TODO: 10/7/17 Another logic should be added: Checking if the cached data is dirty.

            DataServiceCloud.getInstance().readData(new DataSource.LoadDataCallback() {
                @Override
                public void onDataLoaded(List<Item> newItems) {
                    // Cache the data
                    DataServiceLocal.getInstance().cacheItems(newItems);

                    callback.onDataLoaded(newItems);
                }

                @Override
                public void onDataFailed() {
                    callback.onDataFailed();
                }
            });
        } else {
            // Return the cached data.
            callback.onDataLoaded(items);
        }
    }
}
