package net.ruigeng.quinntas.data;

import net.ruigeng.quinntas.models.Item;

import java.util.List;

/**
 * Created by rui.geng on 9/7/17.
 */

public class DataSource {

    public interface LoadDataCallback {
        void onDataLoaded(List<Item> items);
        void onDataFailed();
    }

    void getData(LoadDataCallback callback){}
}
