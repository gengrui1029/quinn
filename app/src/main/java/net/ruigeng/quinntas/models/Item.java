package net.ruigeng.quinntas.models;

/**
 * Created by rui.geng on 5/7/17.
 */

public class Item{
    String title = "";
    String href = "";
    String ingredients = "";
    String thumbnail = "";

    public String getTitle() {
        return title;
    }

    public String getHref() {
        return href;
    }

    public String getIngredients() {
        return ingredients;
    }

    public String getThumbnail() {
        return thumbnail;
    }
}
