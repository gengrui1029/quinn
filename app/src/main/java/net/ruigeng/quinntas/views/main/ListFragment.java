package net.ruigeng.quinntas.views.main;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.ruigeng.quinntas.R;
import net.ruigeng.quinntas.data.DataService;
import net.ruigeng.quinntas.data.DataSource;
import net.ruigeng.quinntas.models.Item;
import net.ruigeng.quinntas.views.BaseFragment;

import java.util.List;

/**
 * Created by rui.geng on 5/7/17.
 */

public class ListFragment extends BaseFragment {

    public static ListFragment getInstance() {
        return new ListFragment();
    }

    private RecyclerView mRecyclerViewList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_list, container, false);

        mRecyclerViewList = (RecyclerView) root.findViewById(R.id.rv_list);
        
        //// TODO: 10/7/17 Add views

        fetchList();
        
        return root;
    }

    /**
     * Fetch the list from data source.
     */
    private void fetchList() {
        DataService.getInstance().getItems(new DataSource.LoadDataCallback() {
            @Override
            public void onDataLoaded(List<Item> items) {

            }

            @Override
            public void onDataFailed() {

            }
        });
    }
}
