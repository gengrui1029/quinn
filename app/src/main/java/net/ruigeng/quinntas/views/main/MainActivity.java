package net.ruigeng.quinntas.views.main;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import net.ruigeng.quinntas.R;
import net.ruigeng.quinntas.views.BaseActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListFragment fragment = ListFragment.getInstance();

        getFragmentManager().beginTransaction()
                .add(R.id.main_layout, fragment)
                .commit();
    }
}
